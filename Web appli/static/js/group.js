var ctx = document.getElementById("myChart");
var ctxS = document.getElementById("mySeuil");
var burger = document.getElementById("menuBurger");
var paramMenu = document.getElementById("param");
var groupMenu = document.getElementById("groupMenu");

var dataHour = [];
var data = {
    labels: dataHour,
    datasets: [
        {
            label: "Capteur 1",
            fill: false,
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "rgba(179,181,198,1)",
            pointBackgroundColor: "rgba(179,181,198,1)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(179,181,198,1)",
            data: setDataArray()
        },
        {
            label: "Capteur 2",
            fill: false,
            backgroundColor: "rgba(255,99,132,0.2)",
            borderColor: "rgba(255,99,132,1)",
            pointBackgroundColor: "rgba(255,99,132,1)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(255,99,132,1)",
            data: setDataArray()
        },
        {
            label: "Capteur 3",
            fill: false,
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "rgb(0, 255, 191)",
            pointBackgroundColor: "rgba(179,181,198,1)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(179,181,198,1)",
            data: setDataArray()
        },
        {
            label: "Capteur 4",
            fill: false,
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "rgb(0, 255, 64)",
            pointBackgroundColor: "rgba(179,181,198,1)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(179,181,198,1)",
            data: setDataArray()
        },
        {
            label: "Capteur 5",
            fill: false,
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "rgb(255, 234, 36)",
            pointBackgroundColor: "rgba(179,181,198,1)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(179,181,198,1)",
            data: setDataArray()
        }
    ]
};
var dataS = {
    labels: ["Capteur 1", "Capteur 2", "Capteur 3", "Capteur 4", "Capteur 5"],
    datasets: [
        {
            label: "Température Maximum",
            backgroundColor: "rgba(255,99,132,0.2)",
            borderColor: "rgba(255,99,132,1)",
            borderWidth: 1,
            hoverBackgroundColor: "rgba(255,99,132,0.4)",
            hoverBorderColor: "rgba(255,99,132,1)",
            data: [65, 59, 80, 81, 56]
        },
        {
            label: "Température Minimum",
            backgroundColor: "rgb(63, 127, 191)",
            borderColor: "rgb(63, 189, 191)",
            borderWidth: 1,
            hoverBackgroundColor: "rgb(73, 189, 191)",
            hoverBorderColor: "rgb(49, 213, 215)",
            data: [25, 19, 40, 41, 16]
        }
    ]
};

for (var i = 0; i < 24 ;i++){
    dataHour.push(i+"h");
}


function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function setDataArray(){
    var data = [];

    for(var i = 0; i < 24; i++){
        data.push(getRandomArbitrary(-10, 25));
    }

    return data;
}

burger.addEventListener('click',function(){

    groupMenu.classList.toggle("closed");
    burger.classList.toggle("glyphicon-menu-hamburger");
    burger.classList.toggle("glyphicon-triangle-bottom")

});

paramMenu.addEventListener('click',function(){
    document.location.href = "Settings"
});

var myChart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

var mySeuil = new Chart(ctxS,{
   type: 'bar',
    data: dataS,
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});



