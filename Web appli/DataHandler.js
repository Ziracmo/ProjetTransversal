var mongodb = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/FH_database';

//-------------SERVEUR MQTT ------------------//
var host = "messagesight.demos.ibm.com";
var port = 1883;

var mqtt    = require('mqtt');
var client  = mqtt.connect({host: host,port: port});

//------------ CODE MQTT----------------//


client.on('connect', function () {
    client.subscribe('ingesupb2/#');
    client.publish('ingesupb2/michel', 'Bonsoir, C\'est Michel');
});


client.on('message', function (topic, message) {
    var date = new Date();

    var second = date.getSeconds();
    var minutes = date.getMinutes();
    var hour = date.getHours();
    var name = topic.replace("ingesupb2/","");

    var instantDate = hour +":"+minutes +":"+ second;
    console.log();
    console.log(name +" - "+ instantDate+" : "+message);

    var sensorData = {
        name:name,
        date:date,
        temp:message
    };

    MongoClient.connect(url, function(err, db) {
        if (err)
            console.log('Unable to connect to the mongoDB server. Error:', err);
        else
            console.log('Connection established to', url);

        db.collection('sensors').insert(sensorData);
        db.collection('sensors').find({name:"groupe4"});

        db.close();

    });


});