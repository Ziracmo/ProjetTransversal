var http = require('http');
var express = require('express');
var app = express();
var fs = require('fs');
var server = http.createServer(app).listen(8080);


//-------------SERVEUR MQTT ------------------//
var host = "messagesight.demos.ibm.com";
var port = 1883;

var mqtt    = require('mqtt');
var client  = mqtt.connect({host: host,port: port});

//------------ CODE MQTT----------------//


client.on('connect', function () {
    client.subscribe('ingesupb2/#');
    client.publish('ingesupb2/michel', 'Bonsoir, C\'est Michel');
});

client.on('message', function (topic, message) {
    var date = new Date();

    var second = date.getSeconds();
    var minutes = date.getMinutes();
    var hour = date.getHours();
    var name = topic.replace("ingesupb2/","");

    var instantDate = hour +":"+minutes +":"+ second;
    console.log();
    console.log(name +" - "+ instantDate+" : "+message);

});

//----------------SELECTION PACKAGE----------------//

app.use(express.static('static'));

//----------------ROUTAGE-----------------//

app.get('/', function(req, res) {
    res.sendFile(__dirname+'index.html');
});

app.get('/Groupes', function(req, res) {
    res.sendFile(__dirname+'/static/views/group.html');
});

app.get('/ChangementMotDePasse', function(req, res) {
    res.sendFile(__dirname+'/static/views/changeMdp.html');
});


app.get('/Settings', function(req, res) {
    res.sendFile(__dirname+'/static/views/settings.html');
});


